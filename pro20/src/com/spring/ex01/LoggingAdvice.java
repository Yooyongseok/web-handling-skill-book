package com.spring.ex01;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

public class LoggingAdvice implements MethodInterceptor // 인터페이스 MethodInterceptor를 구현해 어드바이스 클래스를 만듦
{
	
	@Override
	public Object invoke(MethodInvocation invocation) throws Throwable {
		// 메소드 호출 전 수행 구문
		System.out.print("[메서드 호출 전 : LoggingAdvice ");
		System.out.println(invocation.getMethod() + " 메서드 호출 전 ]");
		
		// invocation 이용 메소드 호출
		Object object = invocation.proceed();
		
		// 메서드 호출 후 수행 구문
		System.out.print("[메소드 호출 후 : loggingAdvice ");
		System.out.println(invocation.getMethod() + " 메소드 호출 후 ]");
		return object;
	}
}
