CREATE TABLE userTBL -- 회원 테이블
(   userID      CHAR(8) NOT NULL PRIMARY KEY,   -- 사용자 아이디(PK)
    userName    NVARCHAR2(10) NOT NULL ,    -- 이름
    birthYear   NUMBER(4) NOT NULL, -- 출생년도
    addr        NCHAR(2) NOT NULL,  -- 지역 ( 경기, 서울, 경남 이런식으로 2글자만 입력 )
    mobile1     CHAR(3),    -- 휴대폰의 국번 ( 010, 011, 016, 017 )
    mobile2     CHAR(8),    -- 휴대폰의 나머지 전화번호 (하이픈 제외)
    height      NUMBER(3),  -- 키
    mDate       DATE    -- 회원가입일
);

CREATE TABLE buyTBL -- 회원 구매 테이블
(   idNum       NUMBER(8) NOT NULL PRIMARY KEY, -- 순번(PK)
    userID      CHAR(8) NOT NULL, -- ID ( FK )
    prodName    NCHAR(6) NOT NULL, -- 물품명
    groupName   NCHAR(4), -- 분류
    price       NUMBER(8) NOT NULL , -- 단가
    amount      NUMBER(3) NOT NULL , -- 수량
    FOREIGN KEY (userID) REFERENCES userTBL(userID)
);