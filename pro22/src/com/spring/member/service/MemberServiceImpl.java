package com.spring.member.service;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.spring.member.dao.MemberDAO;
import com.spring.member.vo.MemberVO;

public class MemberServiceImpl implements MemberService{
	private MemberDAO memberDAO;
	public void setMemberDAO(MemberDAO memberDAO) { 
		this.memberDAO = memberDAO;
		// memberDAO 속성에 setter를 이용, 설정파일에서 생성된 memberDAO를 주입
	}
	
	@Override
	public List listMembers() throws DataAccessException {
		List membersList = null;
		membersList = memberDAO.selectAllMembers();
		return membersList;
	}
	
	@Override
	public void addMember(MemberVO memberVO) {
		memberDAO.addMember(memberVO);
	}
}
