<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%! 
	String name = "이순신";
	public String getName() { return name; }
%>

<% String age = request.getParameter("age"); %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>표현식 연습</title>
</head>
<body>
	<h2>안녕하세요 <%=name %></h2>
	<h2>나이는 <%=age %>이고, 키는 <%=180 %> cm입니다.</h2>
	<h3>10년뒤 제 나이는 <%=Integer.parseInt(age) + 10 %>살 입니다.</h3>
</body>
</html>