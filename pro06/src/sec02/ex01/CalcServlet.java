package sec02.ex01;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/calc")
public class CalcServlet extends HttpServlet {
    
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html ; charset=utf-8");
		
		PrintWriter pw = response.getWriter();
		String command = request.getParameter("command");
		String won = request.getParameter("won");
		String operator = request.getParameter("operator");
		
		if (command != null && command.equals("calculate")) {
			String result = calculate(Float.parseFloat(won), operator);
			pw.print("<html><title>환율 계산 결과</title>");
			pw.print("<font size=5>환율 계산 결과</font><br>");
			pw.print("<font size=10>"+result+""+operator+"</font><br>");
			pw.print("<a href='/pro06/calc'>다시 환율계산 하기</a></html>");
			return;
		}
		
		pw.print("<html><title>환율 계산기</title>");
		pw.print("<font size=5>환율 계산기</font><br>");
		pw.print("<form name='frmCalc' method='get' action='/pro6/calc' />");
		pw.print("원화 : <input type='text' name='won' size=10 /> ");
		pw.print("<select name = 'operator' >");
		pw.print("<option value='dollar'>달러</option>");
		pw.print("<option value='yen'>엔화</option>");
		pw.print("<option value='wian'>위안</option>");
		pw.print("<option value='pound'>파운드</option>");
		pw.print("<option value='euro'>유로</option>");
		pw.print("</select>");
		pw.print("<input type='hidden' name='command' value='calculate' />");
		pw.print("<input type='submit' value='변환' />");
		pw.print("</form>");
		pw.print("</html>");
		pw.close();
	}
	
	private static String calculate(float won, String operator) {
		// 2020-10-15 기준 환율
		final double USD_RATE = 1147.78;
		final double YEN_RATE = 10.90;
		final double CNY_RATE = 170.61;
		final double GBP_RATE = 1483.44;
		final double EUR_RATE = 1343.24;
		
		String result = null;
		
		if(operator.equals("dollar")) {
			result = String.format("%.6f", won / USD_RATE);
		}else if(operator.equals("yen")) {
			result = String.format("%.6f", won / YEN_RATE);
		}else if(operator.equals("wian")) {
			result = String.format("%.6f", won / CNY_RATE);
		}else if(operator.equals("pound")) {
			result = String.format("%.6f", won / GBP_RATE);
		}else if(operator.equals("euro")) {
			result = String.format("%.6f", won / EUR_RATE);
		}
		return result;
	}

}
