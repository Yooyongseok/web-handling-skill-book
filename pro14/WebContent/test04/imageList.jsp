<%@ page language="java" contentType="text/html; charset=UTF-8"
	isELIgnored="false"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
	request.setCharacterEncoding("utf-8");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>이미지 리스트 출력</title>
</head>
<body>
	<ul class="lst_type">
		<li>
			<span style='margin-left:50px'>이미지</span>
			<span>이미지 이름</span>
			<span>선택하기</span>
		</li>
	</ul>
	<c:forEach var="i" begin="1" end="9" step="1">
		<li>
			<a href='#' style='margin-left : 50px'><img src='../image/pepe.png' width='90' height='90' alt='pepe'/></a>
			<a href='#'><strong>이미지 이름 : pepe ${i}</strong></a>
			<a href='#'><input name='chk${i}' type="checkbox"/></a>
		</li>
	</c:forEach>
</body>
</html>