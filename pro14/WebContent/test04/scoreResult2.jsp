<%@ page language="java" contentType="text/html; charset=UTF-8"
	isELIgnored="false"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<% request.setCharacterEncoding("utf-8"); %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>학점 변환 결과</title>
</head>
<body>
	<c:set var="score" value="${param.score}" />
	<h1>
		시험 점수는 <c:out value="${score}"/> ,
	</h1>
	<c:choose>
		<c:when test="${score >= 0 && score <=100 }">
			<c:choose>
				<c:when test="${score >= 90 && score <=100 }">
					<h1> A 학점 임니다!</h1>
				</c:when>
				<c:when test="${score >= 80 && score <90 }">
					<h1> B 학점 임니다!</h1>
				</c:when>
				<c:when test="${score >= 70 && score <80 }">
					<h1> C 학점 임니다!</h1>
				</c:when>
				<c:when test="${score >= 60 && score <70 }">
					<h1> D 학점 임니다!</h1>
				</c:when>
				<c:otherwise>
					<h1>F 학점 , 낙제입니다.</h1>
				</c:otherwise>
			</c:choose>
		</c:when>
		<c:otherwise>
			<h1>점수를 잘못 입력했슈, 입력하지 말지 그랬슈</h1>
			<a href="scoreTest.jsp">점수 입력 다시하러 가기.</a>
		</c:otherwise>
	</c:choose>
</body>
</html>