package com.spring.ex04;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.spring.ex01.MemberVO;

@WebServlet("/mem4.do")
public class MemberServlet extends HttpServlet {
       
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doHandle(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doHandle(request, response);
	}

	private void doHandle(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		MemberDAO memberDAO = new MemberDAO();
		MemberVO memberVO = new MemberVO();
		String action = request.getParameter("action");
		String nextPage = "";
		if(action == null || action.equals("listMembers")) {
			List<MemberVO> membersList = memberDAO.selectAllMemberList();
			request.setAttribute("membersList", membersList);
			nextPage = "test03/listMembers.jsp";
		}else if(action.equals("selectMemberById")) {
			String id = request.getParameter("value");
			memberVO = memberDAO.selectMemberById(id);
			request.setAttribute("memberVO", memberVO);
			nextPage = "test02/memberInfo.jsp";
		}else if(action.equals("selectMemberByPwd")) {
			int pwd = Integer.parseInt(request.getParameter("value"));
			List<MemberVO> membersList= memberDAO.selectMemberByPwd(pwd);
			request.setAttribute("membersList", membersList);
			nextPage = "test03/listMembers.jsp";
		}else if(action.equals("insertMember")) {
			String id = request.getParameter("id");
			String pwd = request.getParameter("pwd");
			String name = request.getParameter("name");
			String email = request.getParameter("email");
			memberVO.setId(id);
			memberVO.setPwd(pwd);
			memberVO.setName(name);
			memberVO.setEmail(email);
			memberDAO.insertMember(memberVO);
			nextPage = "/mem4.do?action=listMembers";
		}else if(action.equals("insertMember2")) {
			String id = request.getParameter("id");
			String pwd = request.getParameter("pwd");
			String name = request.getParameter("name");
			String email = request.getParameter("email");
			// 회원가입창에서 전송된 회원정보를 HashMap에 Key / Value로 저장한후
			// MemberDAO의 insertMember2() 인자로 전달합니다.
			Map<String, String> memberMap = new HashMap();
			memberMap.put("id", id);
			memberMap.put("pwd", pwd);
			memberMap.put("name", name);
			memberMap.put("email", email);
			memberDAO.insertMember2(memberMap);
			nextPage = "/mem4.do?action=listMembers";
		}else if(action.equals("updateMember")) {
			String id = request.getParameter("id");
			String pwd = request.getParameter("pwd");
			String name = request.getParameter("name");
			String email = request.getParameter("email");
			memberVO.setId(id);
			memberVO.setPwd(pwd);
			memberVO.setName(name);
			memberVO.setEmail(email);
			memberDAO.updateMember(memberVO);
			nextPage = "/mem4.do?action=listMembers";
		}else if(action.equals("deleteMember")) {
			String id = request.getParameter("id");
			memberDAO.deleteMember(id);
			nextPage = "/mem4.do?action=listMembers";
		}else if(action.equals("searchMember")) {
			String name = request.getParameter("name");
			String email = request.getParameter("email");
			memberVO.setName(name);
			memberVO.setEmail(email);
			List membersList = memberDAO.searchMember(memberVO);
			request.setAttribute("membersList", membersList);
			nextPage = "test03/listMembers.jsp";
		}else if (action.equals("foreachSelect")) {
			List<String> nameList = new ArrayList();
			nameList.add("홍길동");
			nameList.add("차범근");
			nameList.add("이순신");
			List membersList= memberDAO.foreachSelect(nameList);
			request.setAttribute("membersList", membersList);
			nextPage = "test03/listMembers.jsp";
		}else if (action.equals("foreachInsert")) {
			List<MemberVO> memList = new ArrayList();
			memList.add(new MemberVO("m1","1234","박길동","m1@test.com"));
			memList.add(new MemberVO("m2","1234","이길동","m2@test.com"));
			memList.add(new MemberVO("m3","1234","김길동","m3@test.com"));
			int result = memberDAO.foreachInsert(memList);
			nextPage = "/mem4.do?action=listMembers";
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(nextPage);
		dispatcher.forward(request, response);
	}
}
