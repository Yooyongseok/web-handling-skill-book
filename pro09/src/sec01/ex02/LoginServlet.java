package sec01.ex02;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	public void init() {
		System.out.println("call init method");
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();
		
		String user_id = request.getParameter("user_id");
		String user_pw = request.getParameter("user_pw");
		String user_address = request.getParameter("user_address");
		String user_email = request.getParameter("user_email");
		String user_hp = request.getParameter("user_hp");
		
		String data = "�ȳ��ϼ���!<br> �α����ϼ̽��ϴ�.<br><br>";
		data += "<html><body>";
		data += "���̵� : "+user_id;
		data += "<br>";
		data += "�н����� : "+user_pw;
		data += "<br>";
		data += "�ּ� : "+user_address;
		data += "<br>";
		data += "email : "+user_email;
		data += "<br>";
		data += "�޴���ȭ : "+user_hp;
		data += "</html></body><br>";
		out.println(data);
		
		user_address = URLEncoder.encode(user_address, "utf-8");
		out.println("<a href='/pro09/second?user_id="+user_id+"&user_pw="+user_pw+"&user_address="+user_address+"'>�� ��° �������� ������.</a>");
		
	}
	
	public void destroy() {
		System.out.println("destroy �޼��� ȣ��");
	}
}
