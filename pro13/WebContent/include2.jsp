<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<% request.setCharacterEncoding("utf-8"); %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>include2.jsp</title>
</head>
<body>
	JSP 시작
	<br>
	<jsp:include page="pepe_image.jsp" flush="true" >
		<jsp:param name="name" value="pepe2" />
		<jsp:param name="imgName" value="pepe2.png" />
	</jsp:include>
	<br>
	JSP 끝
</body>
</html>